<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Monitoring extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Log_match_model', 'log_match');
    }

    public function index()
    {
        $data['title'] = 'Monitoring';
        $data['page']  = 'entry/monitoring';

        $this->load->view('templates/app', $data);
    }

    public function show_match()
    {
        $data['title'] = 'Monitoring';
        $data['page']  = 'entry/monitoring_show_match';

        $this->load->view('templates/app', $data);
    }

    public function filter_division($division_id)
    {
        // menyimpan pilihan divisi di session
        $this->session->set_userdata(['division_id' => $division_id]);

        if ($division_id === 'null') {
            $result = $this->log_match->get_all_log_match();
        } else {
            $this->create_final_match_roundrobin($division_id);
            $result = $this->log_match->filter_division($division_id);
        }

        if (count($result) == 0) {
            return $this->send_json_output([], true, 200);
        } else if ($result) {
            return $this->send_json_output($result, true, 200);
        } else {
            return $this->send_json_output("Failed filter data", false, 400);
        }
    }

    public function create_final_match_roundrobin($division_id)
    {
        $this->load->model('Player_division_model', 'player_division');
        $this->player_division->calculate_classement($division_id);

        $result = $this->log_match->create_final_match_roundrobin($division_id);

        if ($result['status']) {
            return $this->send_json_output($result['data'], true, 200);
        } else {
            return $this->send_json_output($result['message'], false, 400);
        }
    }

    public function get_divisions()
    {
        $divisions = $this->monitoring->get_all_divisions();

        if (count($divisions) == 0) {
            return $this->send_json_output([], true, 200);
        } else if ($divisions) {
            return $this->send_json_output($divisions, true, 200);
        } else {
            return $this->send_json_output("Failed get data", false, 400);
        }
    }

    public function get_matches() {
        $matches = $this->monitoring->get_all_matches();

        if (count($matches) == 0) {
            return $this->send_json_output([], true, 200);
        } else if ($matches) {
            return $this->send_json_output($matches, true, 200);
        } else {
            return $this->send_json_output("Failed get data", false, 400);
        }
    }

    public function get_match_started($next = false) {
        $matches = $this->monitoring->get_matchstarted($next);        

        if ($matches['status']) {
            return $this->send_json_output($matches, true, 200);
        } else {
            return $this->send_json_output("Failed get data", false, 400);
        }
    }

    public function get_match_playlist() {
        $matches = $this->monitoring->get_match_division_playlist();

        if (count($matches) == 0) {
            return $this->send_json_output([], true, 200);
        } else if ($matches) {
            return $this->send_json_output($matches, true, 200);
        } else {
            return $this->send_json_output("Failed get data", false, 400);
        }
    }

    public function get_idle_type() {
        $idle = $this->monitoring->get_idle_type();

        if ($idle) {
            return $this->send_json_output($idle, true, 200);
        } else {
            return $this->send_json_output("Failed get data", false, 400);
        }
    }

    public function update_data() {
        $request = parse_post_data();
        
        $dataupdate = [
            'monitoring_idle' => 'VIDEO',
            'monitoring_video' => $request->video,
        ];
        $response = $this->db->update('setting', $dataupdate);
  
        if ($response) {
            $result = [
                'status' => true,
                'data'   => 'success update video',
            ];
        } else {
            $result = [
                'status'  => false,
                'message' => 'failed update video',
            ];
        }

        if ($response) {
            return $this->send_json_output($result['data'], true, 200);
        } else {
            return $this->send_json_output($result['message'], false, 400);
        }
    }

    public function upload_image()
    {
        $upload_image = $_FILES['monitoring_image']['name'];

        if ($upload_image) {
            $result = $this->monitoring->upload_image();
        } else {
            $result = [
                'status'  => false,
                'message' => 'no photo uploaded',
            ];
        }

        if ($result['status']) {
            return $this->send_json_output($result['data'], true, 200);
        } else {
            return $this->send_json_output($result['message'], false, 400);
        }
    }

    public function get_current_match($division_id) {
        $result = $this->monitoring->get_current_match($division_id);

        if ($result['status']) {
            return $this->send_json_output($result['data'], true, 200);
        } else {
            return $this->send_json_output($result['message'], false, 400);
        }
    }
};