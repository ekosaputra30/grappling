<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Monitoring_model extends MY_Model
{
  public function get_current_match($division_id) 
  {
    // error jika parameter tidak lengkap
    if (!$division_id) {
      return [
        'status'  => false,
        'message' => 'Error! division_id or match_system undefined',
      ];
    }

    $this->table = 'log_match';
    $this->where('division_id', $division_id);
    $this->where('match_status', 1);
    $this->order_by('match_number', 'ASC');
    $this->db->limit(1);
    return [
      'status' => true,
      'data' => $this->get_single_array()
    ];
  }
  private function _group_by($column) {
    $this->db->group_by($column);
    return $this;
  }
  public function get_all_divisions() {
    $this->table = 'log_match';
    $this->db->select('log_match.id AS id, division_id, division.division AS division, match_system, match_status');
    $this->db->group_by('division_id');
    $this->join('division');
    $this->where('match_system', 'elimination');
    $this->where('match_status <>', '2');
    $this->order_by('division_id', 'ASC');
    $divisions = $this->get_all_array($this->table);
    $return = [];

    // foreach ($divisions as $key => $division) {
    //   $return['division'][$key] => $division['division_id']
    // }

    return $divisions;
  }

  public function get_division($id)
  {
    $this->where('division_id', $id);
    return $this->get_all_array();
  }

  public function get_all_matches() {
    $this->table = 'log_match';
    $this->where('match_status', '0');
    return $this->get_all_array($this->table);
  }
  public function get_matchstarted($next = 'false') {
    $data = false;
    $last_status = false;
    $status = true;
    $this->_init_row();
    if ($next == 'true') {
      if($n = $this->_check_next_row()) {
        $last_status = $n['last_status'];

        return [
          'status' => $status,
          'last_status' => $last_status,
          'data' => $n['data']
        ];
      }
    }

    $query = $this->db->limit(1)->get_where('queue', ['match_status' => '1', 'flag_status' => '1']);
    $data = $query->row_array();

    return [
      'status' => $status,
      'last_status' => $last_status,
      'data' => $data,
    ];
  }

  private function _init_row() {
    $query = $this->db->get_where('queue', ['match_status' => 1, 'flag_status' => 1]);
    if($query->num_rows() == 0) {
      $first = $this->_check_first_row();
      return $this->db->update('queue', ['flag_status' => 1], ['id' => $first['id']]);
    }

    return false;
  }

  private function _check_current_row() {
    $query = $this->db->limit(1)->get_where('queue', ['match_status' => 1, 'flag_status' => 1]);
    return $query->row_array();
  }

  private function _check_next_row() {
    $laststatus = false;
    $current = $this->_check_current_row();
    $last = $this->_check_last_row();
    $first = $this->_check_first_row();

    $data = $first;

    $q = $this->db->limit(1)->get_where('queue', ['id >' => $current['id']]);
    $next = $q->row_array();
    $id = $next['id'];    
    
    if($current['id'] == $last['id']) {
      $laststatus = true;
      $id = $first['id'];
    }

    $this->_reset_row();

    $this->db->update('queue', ['flag_status'=>1], ['id'=>$id]);

    return [
      'last_status' => $laststatus,
      'data' => $data
    ];
  }

  private function _reset_row() {
    return $this->db->update('queue', ['flag_status'=>0]);
  }

  private function _check_first_row() {
    $query = $this->db->order_by('id', 'asc')->limit(1)->get_where('queue', ['match_status' => 1]);
    return $query->row_array();
  }

  private function _check_last_row() {
    $query = $this->db->order_by('id', 'desc')->limit(1)->get_where('queue', ['match_status' => 1]);
    return $query->row_array();
  }

  public function get_match_division_playlist() {
    $query = $this->db->query("SELECT L.division_id, D.division, COUNT(*) AS count_match
      FROM `log_match` as L 
      JOIN division as D ON D.id = L.division_id
      WHERE L.match_status = 0 GROUP BY L.division_id
    ");

    return $query->result_array();
  }
  
  public function get_idle_type() {
    return $this->db->get('setting')->row_array();
  }

  public function upload_image()
  {
    $config['allowed_types'] = 'gif|jpg|png';
    $config['file_name']     = "bg";
    $config['max_size']      = '10000';
    $config['upload_path']   = './images/monitoring';
    $config['overwrite']     = true;

    $this->load->library('upload', $config);

    if ($this->upload->do_upload('monitoring_image')) {
      $this->db->set('monitoring_idle', 'IMAGE');
      $response = $this->db->update('setting');

      if ($response) {
          $result = [
              'status' => true,
              'data'   => 'success update photo',
          ];
      } else {
          $result = [
              'status'  => false,
              'message' => 'failed update photo',
          ];
      }
    } else {
      $result = [
        'status'  => false,
        'message' => $this->upload->display_errors(),
      ];
    }

    return $result;
  }
}

/* End of file Log_match_model.php */