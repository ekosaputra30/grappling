-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 30 Des 2019 pada 12.18
-- Versi server: 10.4.6-MariaDB
-- Versi PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `grappling`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `access_menu`
--

CREATE TABLE `access_menu` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `access_menu`
--

INSERT INTO `access_menu` (`id`, `role_id`, `menu_id`) VALUES
(1, 1, 1),
(7, 1, 5),
(8, 1, 39),
(9, 1, 40),
(10, 1, 41),
(12, 1, 3),
(17, 1, 44),
(18, 2, 44),
(19, 2, 2),
(21, 1, 2),
(22, 2, 39);

-- --------------------------------------------------------

--
-- Struktur dari tabel `achievement`
--

CREATE TABLE `achievement` (
  `id` int(11) NOT NULL,
  `player_id` int(11) NOT NULL,
  `tournament_name` varchar(255) NOT NULL,
  `division_id` int(11) DEFAULT NULL,
  `winner_position` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `achievement_year` int(11) DEFAULT NULL,
  `category` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `achievement`
--

INSERT INTO `achievement` (`id`, `player_id`, `tournament_name`, `division_id`, `winner_position`, `city_id`, `achievement_year`, `category`) VALUES
(73, 64, 'Tinju Kedungdoro', 1, 2, 2, 2019, 'general'),
(74, 64, 'Tinju Kedungrukem', 1, 2, 2, 2018, 'general'),
(75, 53, 'MMA Master', 8, 1, 3, 2010, 'grappling'),
(76, 18, 'Karate Kyoukushin', 8, 3, 0, 2013, 'general'),
(77, 19, 'Merpati Putih', 1, 2, 0, 2013, 'general'),
(78, 63, 'The King of Kungfu', 8, 1, 0, 2018, 'general'),
(79, 9, 'MMA Champina Championship', 1, 1, 0, 2017, 'general'),
(80, 38, 'Karate Inkai', 1, 1, 0, 2019, 'general'),
(81, 41, 'Joutai Akamoto Championship', 1, 1, 0, 2018, 'general'),
(82, 29, 'Indonesia MMA Championship', 1, 1, 0, 2010, 'general'),
(83, 15, 'Boxing of Death', 1, 2, 0, 2018, 'general'),
(84, 28, 'Fighting Racoon', 1, 1, 0, 2018, 'general'),
(85, 34, 'King of Fighter', 1, 2, 0, 2013, 'general'),
(86, 57, 'Wolrd C Boxing', 1, 1, 0, 2015, 'general'),
(87, 59, 'Inner Punch', 1, 1, 0, 2014, 'general'),
(88, 42, 'Grap End Championship', 1, 2, 0, 2016, 'general'),
(89, 42, 'King Fighter Championship', 1, 1, 0, 2014, 'general'),
(90, 45, 'Boxing Championship', 1, 1, 0, 2013, 'general'),
(91, 31, 'Super Grappler', 1, 1, 2, 2015, 'grappling'),
(92, 48, 'Super Grappler', 1, 1, 2, 2019, 'grappling'),
(93, 63, 'Super Grappler', 12, 1, 2, 2019, 'grappling');

-- --------------------------------------------------------

--
-- Struktur dari tabel `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `city` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `city`
--

INSERT INTO `city` (`id`, `city`) VALUES
(1, 'Jakarta'),
(2, 'Surabaya'),
(3, 'Semarang'),
(4, 'Yogyakarta');

-- --------------------------------------------------------

--
-- Struktur dari tabel `club`
--

CREATE TABLE `club` (
  `id` int(11) NOT NULL,
  `club` varchar(100) NOT NULL,
  `alias` varchar(3) DEFAULT NULL,
  `description` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `club`
--

INSERT INTO `club` (`id`, `club`, `alias`, `description`) VALUES
(1, 'Aji Fighting Club', 'Aji', ''),
(2, 'BJJ Generation', 'BJJ', ''),
(3, 'Mitsu Tomoe', 'Mit', ''),
(4, 'Borneo Banjar Baru', 'Bor', ''),
(5, 'Galaxy', 'Gal', ''),
(6, 'Synergy CMAA', 'Syn', ''),
(7, 'Ksatria', 'Ksa', ''),
(8, 'BJJ Bandung', 'BJJ', ''),
(9, 'Team Bangkok', 'Tea', ''),
(10, 'Amen Fighting Club', 'Ame', ''),
(11, 'Mutant Fighting Art', 'Mut', ''),
(12, 'BJJ Surabaya', 'BJJ', ''),
(13, 'Synergy', 'Syn', ''),
(14, 'Alive Academy', 'Ali', ''),
(15, 'Triple C', 'Tri', ''),
(16, 'Partisan', 'Pts', ''),
(17, 'Universal Grappling', 'U. ', ''),
(18, 'Grappling Academy UGM', 'Gra', ''),
(19, 'Viper Submission', 'Vip', ''),
(20, 'Ragunan Independent', 'R I', ''),
(21, 'Judo Club', 'Jud', ''),
(22, 'PMS Surakarta', 'PMS', ''),
(23, 'MMA Phuket', 'mpu', ''),
(24, 'Permata', 'Per', ''),
(25, 'Team Jando', 'Tea', ''),
(26, 'Synergy Surabaya', 'Syn', ''),
(27, 'Synergy Muara Karang', 'Syn', ''),
(28, 'Synergy Solo', 'Syn', ''),
(29, 'Synergy Cikini', 'Syn', ''),
(30, 'Synergy Kelapa Gading', 'Syn', ''),
(31, 'Synergy Setiabudi', 'Syn', ''),
(32, 'Synergy Grande', 'Syn', ''),
(33, 'Synergy Bali', 'Syn', ''),
(34, 'Synergy Bandung', 'Syn', ''),
(35, 'Reserve', 'Res', ''),
(36, 'Tiger Muay Thai', 'Tig', ''),
(37, 'Spider', 'Spi', ''),
(38, 'Magz', 'Mag', ''),
(39, 'Dayz Kickboxing', 'Day', ''),
(40, 'United Grappling/Synergy', 'Uni', ''),
(41, 'Defense Factory', 'Def', ''),
(42, 'Ragunan SC', 'Rag', ''),
(43, 'Independent Fighter', 'Ind', ''),
(44, 'Untar', 'Unt', ''),
(45, 'Lotus Club', 'Lot', ''),
(46, 'Gold Gym Cikini', 'Gol', ''),
(47, 'Mitsu', 'Mit', ''),
(48, 'No Name', 'No ', ''),
(49, 'Synergy Spider', 'Syn', ''),
(50, 'Synergy Jogja', 'Syn', ''),
(51, 'Tiger Shark', 'Tig', ''),
(52, 'Synergy Pakubuwono', 'Syn', ''),
(53, 'Synergy Bidakara', 'Syn', ''),
(54, 'Synergy Academy', 'Syn', ''),
(55, 'Synergy D Groove', 'Syn', ''),
(56, 'Synergy Anakin', 'Syn', ''),
(57, 'Synergy Pitbull', 'Syn', ''),
(58, 'Alliance', 'All', ''),
(59, 'Kiraumaga', 'Kir', ''),
(60, 'Synergy Dharmahusada', 'Syn', ''),
(61, 'Phoenix Club', 'Pho', ''),
(62, 'Synergy Sawunggaling', 'Syn', ''),
(64, 'Jujitsu I-Kyushin Ryu', 'Juj', ''),
(65, 'Synergy Jiujitsu', 'Syn', ''),
(66, 'Umsida Fight Club', 'Ums', ''),
(67, 'Untag Fighting Club', 'Unt', ''),
(68, 'Pemuda', 'Pem', ''),
(69, '45 Akademi', '45 ', ''),
(70, 'UFC', 'UFC', ''),
(71, 'Universal UGM', 'Uni', ''),
(72, 'DFFC', 'DFF', ''),
(73, 'Airlangga Jujitsu', 'Air', ''),
(74, 'UG Generation', 'UG ', ''),
(75, 'Synergy Syena', 'Syn', ''),
(76, 'White Dragon', 'Whi', ''),
(77, 'Maximum', 'Max', ''),
(78, 'Victory MMA Banyuwangi', 'Vic', ''),
(79, 'Synergy Warrior', 'Syn', ''),
(80, 'Core MMA', 'Cor', ''),
(81, 'JUYA', 'JUY', ''),
(82, 'Warrior Malang', 'War', ''),
(83, 'Synergy Peach', 'Syn', ''),
(84, 'BSA', 'BSA', ''),
(85, 'Synergy Predator Pekanbaru', 'Syn', ''),
(86, 'Synergy Magic Carpet', 'Syn', ''),
(87, 'Banyuwangi MMA', 'Ban', ''),
(88, 'Hellionz', 'Hel', ''),
(89, 'Universal Customs', 'Uni', ''),
(90, 'CDO', 'CDO', ''),
(91, 'Punch MMA', 'Pun', ''),
(92, 'Synergy Punch', 'Syn', ''),
(93, 'Pitbull Academy', 'Pit', ''),
(94, 'Yt Jiu Jitsu', 'Yt ', ''),
(95, 'Jujitsu Sidoarjo', 'Juj', ''),
(96, 'Synergy Lutador', 'Syn', ''),
(97, 'Gresik Fighting Club', 'Gre', ''),
(98, 'Dogo Argentino', 'Dog', ''),
(99, 'GF Team Indonesia', 'GF ', ''),
(100, 'Warrior', 'War', ''),
(101, 'Dragon Fire', 'Dra', ''),
(102, 'Magic Carpet', 'Mag', ''),
(103, 'Alliance Jiujitsu', 'All', ''),
(104, 'Warrior Bekasi', 'War', ''),
(105, 'Escobar BJJ', 'Esc', ''),
(106, 'Han Fight Academy', 'Han', ''),
(107, 'Jakarta Muay Thai', 'Jak', ''),
(108, 'Universal Grappling UMB', ' Gr', ''),
(109, 'Dicky Radicta Fight Team Medan', 'Dic', ''),
(110, 'Peniel Grappling Club', 'Pen', ''),
(111, 'Elite TC', 'Eli', ''),
(112, 'Onyx Synergy', 'Ony', ''),
(113, 'Onyx Synergy Singapore', 'Ony', ''),
(114, 'Impact', 'Imp', ''),
(115, 'MMA Elite', 'MMA', ''),
(116, 'UG Diablo', 'UG ', ''),
(117, 'JMC', 'JMC', ''),
(118, 'Synergy Warrior Bekasi', 'War', ''),
(119, 'Silverback', 'Sil', ''),
(120, 'Aria Adi Survival', 'Ari', ''),
(121, 'Synergy Warriror Bogor', 'War', ''),
(122, 'Ataides Nova Uniao', 'Ata', ''),
(123, 'Cibinonk MMA', 'Cib', ''),
(124, 'Synergy Viper Yogya', 'Syn', ''),
(125, 'Yuri Amadin Lutador', 'Yur', ''),
(126, 'Double Rise', 'Dou', ''),
(127, 'Jakarta MMA', 'Jak', ''),
(128, 'Syena', 'Sye', ''),
(129, 'GFC', 'GFC', ''),
(130, 'GMA Academy', 'GMA', ''),
(131, 'Flash', 'Fla', ''),
(132, 'Synergy Rumble', 'Syn', ''),
(133, 'Hardianism', 'Har', ''),
(134, 'Yogya RS Club', 'Yog', ''),
(135, 'Bastian Jiujitsu', 'Bas', ''),
(136, 'Synergy BP Semarang', 'Syn', ''),
(137, 'Akademi Kristus Bali', 'Aka', ''),
(138, 'Synergy Battle Point', 'Syn', ''),
(139, 'Beston Gracie Bali', 'Bes', ''),
(140, 'Customs MMA', 'Cus', ''),
(141, 'Sington UG', 'Sin', ''),
(142, 'Arena MMA', 'Are', ''),
(143, 'Nerium', 'Ner', ''),
(144, 'Spartan Umsida', 'Spa', ''),
(145, 'Punch Generation', 'Pun', ''),
(146, 'Checkmat Indonesia', 'Che', ''),
(147, 'Han Academy', 'Han', ''),
(148, 'Checkmat Leke Machado', 'Che', ''),
(149, 'Semesta Combat Muaythai', 'Sem', ''),
(150, 'Ijukai Combat Base', 'Iju', ''),
(151, 'Devil Advocates', 'Dev', ''),
(152, 'Synergy Hardiansm', 'Syn', ''),
(153, 'Impact Hardiansm', 'Imp', ''),
(154, 'Rebellion', 'Reb', ''),
(155, 'Synergy Warrior Malang', 'Syn', ''),
(156, 'Warrior Lutalivre', 'War', ''),
(157, 'Shaka Academy', 'Sha', ''),
(158, 'Bastian Academy', 'Bas', ''),
(159, 'Han Al Irsyad', 'Han', ''),
(160, 'Team Judas', 'Tea', ''),
(161, 'Synergy KIX', 'Syn', ''),
(162, 'Werewolf Martial Art Jogja', 'Wer', ''),
(163, 'Synergy MMA', 'Syn', ''),
(164, 'X(TEAM) Ju-Jitsu', 'X(T', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `country` varchar(100) NOT NULL,
  `alias` varchar(3) DEFAULT NULL,
  `img` varchar(100) DEFAULT NULL,
  `description` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `country`
--

INSERT INTO `country` (`id`, `country`, `alias`, `img`, `description`) VALUES
(1, 'Indonesia', 'INA', NULL, ''),
(2, 'Australia', 'AUS', NULL, ''),
(3, 'Belanda', 'NED', NULL, ''),
(4, 'Belgium', 'BLG', NULL, ''),
(5, 'Brazil', 'BRA', NULL, ''),
(6, 'Canada', 'CAN', NULL, ''),
(7, 'Denmark', 'DEN', NULL, ''),
(8, 'Philippines', 'PHI', NULL, ''),
(9, 'Hongkong', 'HKG', NULL, ''),
(10, 'India', 'IND', NULL, ''),
(11, 'England', 'ENG', NULL, ''),
(12, 'Italy', 'ITA', NULL, ''),
(13, 'Japan', 'JPN', NULL, ''),
(14, 'Germany', 'GER', NULL, ''),
(15, 'South Korea', 'KOR', NULL, ''),
(16, 'Malaysia', 'MAS', NULL, ''),
(17, 'New Zealand', 'NZE', NULL, ''),
(18, 'Norway', 'NWY', NULL, ''),
(19, 'France', 'FRA', NULL, ''),
(20, 'Portugal', 'POR', NULL, ''),
(21, 'Singapore', 'SIN', NULL, ''),
(22, 'Scottland', 'SCO', NULL, ''),
(23, 'Spain', 'SPA', NULL, ''),
(24, 'Sweden', 'SWE', NULL, ''),
(25, 'Switzerland', 'SWS', NULL, ''),
(26, 'Thailand', 'THA', NULL, ''),
(27, 'USA', 'USA', NULL, ''),
(28, 'Vietnam', 'VIE', NULL, ''),
(29, 'Wales', 'WAL', NULL, ''),
(31, 'Timor Leste', 'Tim', NULL, ''),
(32, 'Liberia', 'LIB', NULL, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `division`
--

CREATE TABLE `division` (
  `id` int(11) NOT NULL,
  `division` varchar(100) NOT NULL,
  `min_weight` double NOT NULL,
  `max_weight` double NOT NULL,
  `gender` varchar(20) NOT NULL,
  `system` varchar(30) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `play` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `division`
--

INSERT INTO `division` (`id`, `division`, `min_weight`, `max_weight`, `gender`, `system`, `description`, `play`) VALUES
(1, 'Division 0 kg - 54 kg', 0, 54, 'male', '', '', '0'),
(2, 'Division 55 kg - 65.99 kg', 55, 65.99, 'male', '', '', '0'),
(3, 'Division 66 kg - 76.99 kg', 66, 76.99, 'male', '', '', '1'),
(4, 'Division 77 kg - 87.99 kg', 77, 87.99, 'male', '', '', '0'),
(8, 'Semifinal < 54', 0, 55, 'male', '', '', '0'),
(10, 'ABSOLUTE', 0, 200, 'all', '', '', '1'),
(11, 'Semifinal 77-87', 77, 87.99, 'male', '', '', '0'),
(12, 'Woman', 0, 200, 'female', '', '', '0'),
(63, 'Division 33 kg - 44 kg', 33, 44, 'male', NULL, NULL, '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `log_match`
--

CREATE TABLE `log_match` (
  `id` int(11) NOT NULL,
  `division_id` int(11) NOT NULL,
  `winning_id` int(11) DEFAULT NULL,
  `referee_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `city` varchar(128) DEFAULT NULL,
  `match_number` int(11) DEFAULT NULL,
  `match_system` varchar(128) DEFAULT NULL,
  `match_index` int(11) DEFAULT NULL,
  `pool_number` varchar(1) DEFAULT NULL,
  `pd1_id` int(11) DEFAULT NULL,
  `pd1_redcard` int(11) DEFAULT 0,
  `pd1_yellowcard` int(11) DEFAULT 0,
  `pd1_greencard` int(11) DEFAULT 0,
  `pd1_point` int(11) DEFAULT 0,
  `pd2_id` int(11) DEFAULT NULL,
  `pd2_redcard` int(11) DEFAULT 0,
  `pd2_yellowcard` int(11) DEFAULT 0,
  `pd2_greencard` int(11) DEFAULT 0,
  `pd2_point` int(11) DEFAULT 0,
  `winner` int(11) DEFAULT NULL,
  `next_match` varchar(20) DEFAULT NULL,
  `match_status` varchar(1) DEFAULT '0',
  `time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `log_match`
--

INSERT INTO `log_match` (`id`, `division_id`, `winning_id`, `referee_id`, `date`, `city`, `match_number`, `match_system`, `match_index`, `pool_number`, `pd1_id`, `pd1_redcard`, `pd1_yellowcard`, `pd1_greencard`, `pd1_point`, `pd2_id`, `pd2_redcard`, `pd2_yellowcard`, `pd2_greencard`, `pd2_point`, `winner`, `next_match`, `match_status`, `time`) VALUES
(1865, 10, NULL, NULL, NULL, NULL, 1, 'elimination', 1, NULL, 20, 0, 0, 0, 0, 57, 0, 0, 0, 0, NULL, '2.1', '0', NULL),
(1866, 10, NULL, NULL, NULL, NULL, 2, 'elimination', 1, NULL, 55, 0, 0, 0, 0, 77, 0, 0, 0, 0, NULL, '2.1', '0', NULL),
(1867, 10, NULL, NULL, NULL, NULL, 3, 'elimination', 1, NULL, 21, 0, 0, 0, 0, 65, 0, 0, 0, 0, NULL, '2.2', '0', NULL),
(1868, 10, NULL, NULL, NULL, NULL, 4, 'elimination', 1, NULL, 70, 0, 0, 0, 0, 42, 0, 0, 0, 0, NULL, '2.2', '0', NULL),
(1869, 10, NULL, NULL, NULL, NULL, 5, 'elimination', 1, NULL, 58, 0, 0, 0, 0, 64, 0, 0, 0, 0, NULL, '2.3', '0', NULL),
(1870, 10, NULL, NULL, NULL, NULL, 6, 'elimination', 1, NULL, 68, 0, 0, 0, 0, 63, 0, 0, 0, 0, NULL, '2.3', '0', NULL),
(1871, 10, NULL, NULL, NULL, NULL, 7, 'elimination', 1, NULL, 67, 0, 0, 0, 0, 62, 0, 0, 0, 0, NULL, '2.4', '0', NULL),
(1872, 10, NULL, NULL, NULL, NULL, 8, 'elimination', 1, NULL, 54, 0, 0, 0, 0, 60, 0, 0, 0, 0, NULL, '2.4', '0', NULL),
(1873, 10, NULL, NULL, NULL, NULL, 9, 'elimination', 1, NULL, 73, 0, 0, 0, 0, 75, 0, 0, 0, 0, NULL, '2.5', '0', NULL),
(1874, 10, NULL, NULL, NULL, NULL, 10, 'elimination', 1, NULL, 61, 0, 0, 0, 0, 74, 0, 0, 0, 0, NULL, '2.5', '0', NULL),
(1875, 10, NULL, NULL, NULL, NULL, 11, 'elimination', 1, NULL, 72, 0, 0, 0, 0, 22, 0, 0, 0, 0, NULL, '2.6', '0', NULL),
(1876, 10, NULL, NULL, NULL, NULL, 12, 'elimination', 1, NULL, 71, 0, 0, 0, 0, 59, 0, 0, 0, 0, NULL, '2.6', '0', NULL),
(1877, 10, NULL, NULL, NULL, NULL, 13, 'elimination', 1, NULL, 18, 0, 0, 0, 0, 56, 0, 0, 0, 0, NULL, '2.7', '0', NULL),
(1878, 10, NULL, NULL, NULL, NULL, 14, 'elimination', 1, NULL, 76, 0, 0, 0, 0, 44, 0, 0, 0, 0, NULL, '2.7', '0', NULL),
(1879, 10, NULL, NULL, NULL, NULL, 15, 'elimination', 1, NULL, 43, 0, 0, 0, 0, 41, 0, 0, 0, 0, NULL, '2.8', '0', NULL),
(1880, 10, NULL, NULL, NULL, NULL, 16, 'elimination', 1, NULL, 69, 0, 0, 0, 0, NULL, 0, 0, 0, 0, -1, '2.8', '2', NULL),
(1881, 10, NULL, NULL, NULL, NULL, 1, 'elimination', 2, NULL, NULL, 0, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, '3.1', '0', NULL),
(1882, 10, NULL, NULL, NULL, NULL, 2, 'elimination', 2, NULL, NULL, 0, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, '3.1', '0', NULL),
(1883, 10, NULL, NULL, NULL, NULL, 3, 'elimination', 2, NULL, NULL, 0, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, '3.2', '0', NULL),
(1884, 10, NULL, NULL, NULL, NULL, 4, 'elimination', 2, NULL, NULL, 0, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, '3.2', '0', NULL),
(1885, 10, NULL, NULL, NULL, NULL, 5, 'elimination', 2, NULL, NULL, 0, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, '3.3', '0', NULL),
(1886, 10, NULL, NULL, NULL, NULL, 6, 'elimination', 2, NULL, NULL, 0, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, '3.3', '0', NULL),
(1887, 10, NULL, NULL, NULL, NULL, 7, 'elimination', 2, NULL, NULL, 0, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, '3.4', '0', NULL),
(1888, 10, NULL, NULL, NULL, NULL, 8, 'elimination', 2, NULL, 69, 0, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, '3.4', '0', NULL),
(1889, 10, NULL, NULL, NULL, NULL, 1, 'elimination', 3, NULL, NULL, 0, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, '4.1', '0', NULL),
(1890, 10, NULL, NULL, NULL, NULL, 2, 'elimination', 3, NULL, NULL, 0, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, '4.1', '0', NULL),
(1891, 10, NULL, NULL, NULL, NULL, 3, 'elimination', 3, NULL, NULL, 0, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, '4.2', '0', NULL),
(1892, 10, NULL, NULL, NULL, NULL, 4, 'elimination', 3, NULL, NULL, 0, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, '4.2', '0', NULL),
(1893, 10, NULL, NULL, NULL, NULL, 1, 'elimination', 4, NULL, NULL, 0, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, '5.1', '0', NULL),
(1894, 10, NULL, NULL, NULL, NULL, 2, 'elimination', 4, NULL, NULL, 0, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, '5.1', '0', NULL),
(1895, 10, NULL, NULL, NULL, NULL, 1, 'elimination', 5, NULL, NULL, 0, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, '6.1', '0', NULL),
(1896, 10, NULL, NULL, NULL, NULL, 0, 'elimination', 5, NULL, NULL, 0, 0, 0, 0, NULL, 0, 0, 0, 0, NULL, NULL, '0', NULL),
(1961, 1, 10, 14, NULL, NULL, NULL, 'roundrobin', 1, 'B', 7, 0, 0, 0, 0, 36, 0, 0, 0, 0, 7, NULL, '2', 569),
(1962, 1, 9, 14, NULL, NULL, NULL, 'roundrobin', 2, 'B', 7, 0, 0, 0, 0, 5, 0, 0, 0, 0, 5, NULL, '2', 380),
(1963, 1, 3, NULL, NULL, NULL, NULL, 'roundrobin', 3, 'B', 5, 0, 0, 0, 0, 36, 0, 0, 0, 0, 36, NULL, '2', 1462),
(1964, 1, 7, 13, NULL, NULL, NULL, 'roundrobin', 1, 'A', 16, 0, 0, 0, 0, 8, 0, 0, 0, 0, 16, NULL, '2', 4731),
(1965, 1, 10, 10, NULL, NULL, NULL, 'roundrobin', 2, 'A', 16, 0, 0, 0, 0, 30, 0, 0, 0, 0, 16, NULL, '2', 0),
(1966, 1, 9, 14, NULL, NULL, NULL, 'roundrobin', 3, 'A', 8, 0, 0, 0, 0, 30, 0, 0, 0, 0, 30, NULL, '2', 599),
(1967, 1, 10, 13, NULL, NULL, 1, 'roundrobin', 1, NULL, 5, 0, 0, 0, 0, 16, 0, 0, 0, 0, 5, NULL, '2', 0),
(1968, 1, 10, 14, NULL, NULL, 2, 'roundrobin', 1, NULL, 7, 0, 0, 0, 0, 30, 0, 0, 0, 0, 7, NULL, '2', 458),
(1977, 12, NULL, NULL, NULL, NULL, 1, 'elimination', 1, NULL, 13, 0, 0, 0, 0, NULL, 0, 0, 0, 0, -1, '2.1', '2', NULL),
(1978, 12, 9, 13, NULL, NULL, 2, 'elimination', 1, NULL, 79, 0, 0, 0, 0, 12, 0, 0, 0, 0, 79, '2.1', '2', 369),
(1979, 12, NULL, NULL, NULL, NULL, 3, 'elimination', 1, NULL, 78, 0, 0, 0, 0, NULL, 0, 0, 0, 0, -1, '2.2', '2', NULL),
(1980, 12, NULL, NULL, NULL, NULL, 4, 'elimination', 1, NULL, 46, 0, 0, 0, 0, NULL, 0, 0, 0, 0, -1, '2.2', '2', NULL),
(1981, 12, 7, 12, NULL, NULL, 1, 'elimination', 2, NULL, 13, 0, 0, 0, 0, 79, 0, 0, 0, 0, 13, '3.1', '2', 0),
(1982, 12, 10, 14, NULL, NULL, 2, 'elimination', 2, NULL, 78, 0, 0, 0, 0, 46, 0, 0, 0, 0, 46, '3.1', '2', 615),
(1983, 12, 9, 13, NULL, NULL, 1, 'elimination', 3, NULL, 13, 0, 0, 0, 0, 46, 0, 0, 0, 0, 46, '4.1', '2', 0),
(1984, 12, 10, 14, NULL, NULL, 0, 'elimination', 3, NULL, 79, 0, 0, 0, 0, 78, 0, 0, 0, 0, 79, NULL, '2', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `menu` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `menu`
--

INSERT INTO `menu` (`id`, `menu`) VALUES
(1, 'Admin'),
(2, 'Profile'),
(3, 'Sidebar'),
(39, 'Master'),
(40, 'Entry'),
(41, 'Setting');

-- --------------------------------------------------------

--
-- Struktur dari tabel `player`
--

CREATE TABLE `player` (
  `id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `club_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `nickname` varchar(10) DEFAULT NULL,
  `gender` varchar(20) NOT NULL,
  `height` double DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `achievement` text NOT NULL,
  `left_photo` varchar(255) DEFAULT NULL,
  `right_photo` varchar(255) DEFAULT NULL,
  `front_photo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `player`
--

INSERT INTO `player` (`id`, `country_id`, `club_id`, `name`, `nickname`, `gender`, `height`, `weight`, `achievement`, `left_photo`, `right_photo`, `front_photo`) VALUES
(1, 1, 133, 'Verontino F.G', 'Ver', 'male', NULL, 65.7, '999', NULL, NULL, NULL),
(2, 1, 133, 'Sofian', 'Sof', 'male', NULL, 68.8, '999', NULL, NULL, NULL),
(3, 1, 73, 'Muhammad Faqihufiddin', 'Muh', 'male', NULL, 70.6, '999', NULL, NULL, NULL),
(4, 1, 33, 'Jay Gusti', 'Jay', 'male', NULL, 74.4, '999', 'player_4_left_photo.jpg', 'player_4_right_photo.jpg', 'player_4_front_photo.jpg'),
(5, 1, 133, 'Jerome S. Paye', 'Jer', 'male', NULL, 65.1, '999', NULL, NULL, NULL),
(6, 1, 73, 'Nofianto Rachmad H', 'Nof', 'male', NULL, 65.2, '999', NULL, NULL, NULL),
(7, 1, 133, 'Rangga Raphael', 'Ran', 'male', NULL, 63.5, '999', 'player_7_left_photo.jpg', 'player_7_right_photo.jpg', 'player_7_front_photo.jpg'),
(8, 1, 133, 'Jevon Joshua', 'Jev', 'male', NULL, 53.4, '999', 'player_8_left_photo.jpg', 'player_8_right_photo.jpg', 'player_8_front_photo.jpg'),
(9, 1, 73, 'Achmad Muhaimin', 'Ach', 'male', NULL, 54.8, '999', 'player_9_left_photo1.jpg', 'player_9_right_photo1.jpg', 'player_9_front_photo1.jpg'),
(11, 1, 99, 'Christopher Andrew Wibisono', 'Chr', 'male', NULL, 101, '999', NULL, NULL, NULL),
(12, 1, 160, 'Henrix Setyawan', 'Hen', 'male', NULL, 90.5, '999', 'player_12_left_photo.jpg', 'player_12_right_photo.jpg', 'player_12_front_photo.jpg'),
(13, 1, 160, 'Fani Maulid Iman', 'Fan', 'male', NULL, 72.7, '999', NULL, NULL, NULL),
(14, 1, 160, 'Nicholas Lukmanto', 'Nic', 'male', NULL, 69.3, '999', 'player_14_left_photo.jpg', 'player_14_right_photo.jpg', 'player_14_front_photo.jpg'),
(15, 1, 99, 'Alfonsus Michael', 'Alf', 'male', NULL, 74.5, '999', 'player_15_left_photo.jpg', 'player_15_right_photo.jpg', 'player_15_front_photo.jpg'),
(16, 27, 58, 'Tommy White', 'Tom', 'male', NULL, 64.7, '999', 'player_16_left_photo.jpg', 'player_16_right_photo.jpg', 'player_16_front_photo.jpg'),
(17, 1, 93, 'Hanny Stefhan', 'Han', 'male', NULL, 113.8, '999', NULL, NULL, NULL),
(18, 1, 137, 'Indri Tristianti', 'Ind', 'female', NULL, 49.3, '999', 'player_18_left_photo.jpg', 'player_18_right_photo.jpg', 'player_18_front_photo.jpg'),
(19, 1, 2, 'Michelle', 'Mic', 'female', NULL, 99.8, '999', 'player_19_left_photo1.jpg', 'player_19_right_photo1.jpg', 'player_19_front_photo.jpg'),
(20, 1, 133, 'Sugiarto', 'Sug', 'male', NULL, 65.9, '999', 'player_20_left_photo.jpg', 'player_20_right_photo.jpg', 'player_20_front_photo.jpg'),
(21, 1, 133, 'Ivan Winarto', 'Iva', 'male', NULL, 68.4, '999', NULL, NULL, NULL),
(22, 1, 58, 'Prananda', 'Pra', 'male', NULL, 62.3, '999', NULL, NULL, NULL),
(23, 1, 137, 'Masuria', 'Mas', 'male', NULL, 59.6, '999', NULL, NULL, NULL),
(24, 1, 137, 'Theja Yudi Wijaya', 'The', 'male', NULL, 64.4, '999', 'player_24_left_photo.jpg', NULL, NULL),
(25, 1, 58, 'Stefanus Budianto', 'Ste', 'male', NULL, 85.5, '999', NULL, NULL, NULL),
(26, 1, 161, 'Heath Rewah', 'Hea', 'male', NULL, 68.1, '999', NULL, NULL, NULL),
(27, 1, 2, 'Tjong Steven S', 'Tjo', 'male', NULL, 75.7, '999', NULL, NULL, NULL),
(28, 1, 2, 'Andre Chandra', 'And', 'male', NULL, 71.8, '999', 'player_28_left_photo.jpg', 'player_28_right_photo.jpg', 'player_28_front_photo.jpg'),
(29, 1, 26, 'Albert Junius Tavvic', 'Alb', 'male', NULL, 68.1, '999', NULL, NULL, NULL),
(30, 1, 160, 'Redam Guruh', 'Red', 'male', NULL, 64.6, '999', NULL, NULL, NULL),
(31, 1, 58, 'Rudi Mila', 'Rud', 'male', NULL, 51.4, '999', 'player_31_left_photo.jpg', 'player_31_right_photo.jpg', 'player_31_front_photo.jpg'),
(32, 1, 99, 'Marcellinus Alberto Halim', 'Mar', 'male', NULL, 61.7, '999', NULL, NULL, NULL),
(33, 1, 26, 'Eric Arman Hadi', 'Eri', 'male', NULL, 111.1, '999', NULL, NULL, NULL),
(34, 1, 99, 'Andrian Febrianto', 'And', 'male', NULL, 100.5, '999', NULL, NULL, NULL),
(35, 1, 2, 'Bryan', 'Bry', 'male', NULL, 66.9, '999', NULL, NULL, NULL),
(36, 1, 26, 'Yohanes Ryan', 'Yoh', 'male', NULL, 64.8, '999', 'player_36_left_photo.jpg', 'player_36_right_photo.jpg', 'player_36_front_photo.jpg'),
(37, 1, 162, 'Christian K.S', 'Chr', 'male', NULL, 114.3, '999', NULL, NULL, NULL),
(38, 1, 162, 'Adhi Putra', 'Adh', 'male', NULL, 49.2, '999', 'player_38_left_photo.jpg', 'player_38_right_photo.jpg', 'player_38_front_photo.jpg'),
(39, 1, 99, 'William Eka Putra', 'Wil', 'male', NULL, 73.1, '999', 'player_39_left_photo.jpg', 'player_39_right_photo.jpg', 'player_39_front_photo.jpg'),
(40, 19, 163, 'Julien Salet', 'Jul', 'male', NULL, 86.5, '999', NULL, NULL, NULL),
(41, 1, 99, 'Ahmad Ritauddin', 'Ahm', 'male', NULL, 71.5, '999', 'player_41_left_photo.jpg', 'player_41_right_photo.jpg', 'player_41_front_photo.jpg'),
(42, 1, 99, 'Ardilon R Faruwu', 'Ard', 'male', NULL, 82.8, '999', NULL, NULL, NULL),
(43, 1, 99, 'Rio', 'Rio', 'male', NULL, 74.4, '999', NULL, NULL, NULL),
(44, 1, 72, 'Ricky Nugroho', 'Ric', 'male', NULL, 55.3, '999', 'player_44_left_photo.jpg', 'player_44_right_photo.jpg', 'player_44_front_photo.jpg'),
(45, 1, 72, 'Arfan Nando', 'Arf', 'male', NULL, 57.8, '999', NULL, NULL, NULL),
(47, 1, 72, 'Roni T.S', 'Ron', 'male', NULL, 51.7, '999', 'player_47_left_photo.jpg', 'player_47_right_photo.jpg', 'player_47_front_photo.jpg'),
(48, 1, 72, 'Girindra W', 'Gir', 'male', NULL, 51.2, '999', 'player_48_left_photo.jpg', 'player_48_right_photo.jpg', 'player_48_front_photo.jpg'),
(49, 1, 26, 'Muhammad Aldy Dharmawan', 'Ald', 'male', NULL, 85.9, '999', 'player_49_left_photo.jpg', 'player_49_right_photo.jpg', 'player_49_front_photo.jpg'),
(50, 1, 148, 'Felix', 'Fel', 'male', NULL, 69.8, '999', NULL, NULL, NULL),
(51, 1, 26, 'Findi', 'Fin', 'male', NULL, 83.3, '999', NULL, NULL, NULL),
(52, 1, 99, 'Bonny Baskoro', 'Bon', 'male', NULL, 50, '999', 'player_52_left_photo.jpg', 'player_52_right_photo.jpg', 'player_52_front_photo.jpg'),
(53, 1, 99, 'Christine Bayu', 'Chr', 'female', NULL, 59.1, '999', 'player_53_left_photo1.jpg', 'player_53_right_photo.jpg', 'player_53_front_photo1.jpg'),
(55, 1, 164, 'Nanang Rifai', 'Nan', 'male', NULL, 73.8, '999', NULL, NULL, NULL),
(56, 1, 26, 'Richard Ericsson', 'Ric', 'male', NULL, 87.7, '999', NULL, NULL, NULL),
(57, 1, 26, 'Anggit Karisma Yudha', 'Ang', 'male', NULL, 71.9, '999', NULL, NULL, NULL),
(58, 1, 26, 'Roy Kurniawan Laksono', 'Roy', 'male', NULL, 75, '999', NULL, NULL, NULL),
(59, 1, 162, 'Anugrahesa Tegar', 'Anu', 'male', NULL, 75.7, '999', NULL, NULL, NULL),
(60, 1, 99, 'Yohanes Putra Santoso', 'Yoh', 'male', NULL, 94.6, '999', NULL, NULL, NULL),
(61, 1, 26, 'Zefa', 'Zef', 'male', NULL, 60, '999', NULL, NULL, NULL),
(63, 1, 133, 'Monica Seles', 'Monica', 'female', 145, 67.8, '999', 'player_63_left_photo1.jpg', 'player_63_right_photo1.jpg', 'player_63_front_photo.jpg'),
(64, 1, 99, 'Angela Gunadiii', 'Angel', 'female', 190, 61.6, '999', 'player_64_left_photo1.jpg', 'player_64_right_photo1.jpg', 'player_64_front_photo.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `player_division`
--

CREATE TABLE `player_division` (
  `id` int(11) NOT NULL,
  `division_id` int(11) NOT NULL,
  `player_id` int(11) NOT NULL,
  `pool_number` varchar(20) DEFAULT NULL,
  `win` int(11) DEFAULT 0,
  `draw` int(11) DEFAULT 0,
  `lose` int(11) DEFAULT 0,
  `pool_winner` varchar(1) DEFAULT '0',
  `total_time` int(11) NOT NULL,
  `division_winner` varchar(20) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `player_division`
--

INSERT INTO `player_division` (`id`, `division_id`, `player_id`, `pool_number`, `win`, `draw`, `lose`, `pool_winner`, `total_time`, `division_winner`) VALUES
(5, 1, 48, 'B', 1, 0, 1, '1', 380, '1'),
(7, 1, 47, 'B', 1, 0, 1, '2', 569, '3'),
(8, 1, 38, 'A', 0, 0, 2, '0', 0, '0'),
(12, 12, 18, 'A', 0, 0, 0, '0', 0, '0'),
(13, 12, 19, 'A', 0, 0, 0, '0', 0, '2'),
(16, 1, 31, 'A', 2, 0, 0, '1', 4731, '2'),
(18, 10, 45, NULL, 0, 0, 0, '0', 0, '0'),
(20, 10, 28, NULL, 0, 0, 0, '0', 0, '0'),
(21, 10, 27, NULL, 0, 0, 0, '0', 0, '0'),
(22, 10, 12, NULL, 0, 0, 0, '0', 0, '0'),
(27, 11, 49, 'A', 0, 0, 0, '0', 0, '0'),
(30, 1, 52, 'A', 1, 0, 1, '2', 599, '0'),
(31, 11, 56, 'A', 0, 0, 0, '0', 0, '0'),
(32, 11, 25, 'B', 0, 0, 0, '0', 0, '0'),
(33, 11, 42, 'B', 0, 0, 0, '0', 0, '0'),
(34, 11, 40, 'B', 0, 0, 0, '0', 0, '0'),
(35, 11, 51, 'A', 0, 0, 0, '0', 0, '0'),
(36, 1, 8, 'B', 1, 0, 1, '0', 1462, '0'),
(38, 11, 23, 'A', 0, 0, 0, '0', 0, '0'),
(39, 11, 4, 'A', 0, 0, 0, '0', 0, '0'),
(40, 11, 43, 'B', 0, 0, 0, '0', 0, '0'),
(41, 10, 38, NULL, 0, 0, 0, '0', 0, '0'),
(42, 10, 41, NULL, 0, 0, 0, '0', 0, '0'),
(43, 10, 9, NULL, 0, 0, 0, '0', 0, '0'),
(44, 10, 59, NULL, 0, 0, 0, '0', 0, '0'),
(46, 12, 63, 'A', 0, 0, 0, '0', 0, '1'),
(47, 8, 9, NULL, 0, 0, 0, '0', 0, '0'),
(48, 8, 38, NULL, 0, 0, 0, '0', 0, '0'),
(49, 8, 52, NULL, 0, 0, 0, '0', 0, '0'),
(50, 8, 48, NULL, 0, 0, 0, '0', 0, '0'),
(51, 8, 8, NULL, 0, 0, 0, '0', 0, '0'),
(52, 8, 47, NULL, 0, 0, 0, '0', 0, '0'),
(53, 8, 31, NULL, 0, 0, 0, '0', 0, '0'),
(54, 10, 29, NULL, 0, 0, 0, '0', 0, '0'),
(55, 10, 35, NULL, 0, 0, 0, '0', 0, '0'),
(56, 10, 37, NULL, 0, 0, 0, '0', 0, '0'),
(57, 10, 11, NULL, 0, 0, 0, '0', 0, '0'),
(58, 10, 33, NULL, 0, 0, 0, '0', 0, '0'),
(59, 10, 13, NULL, 0, 0, 0, '0', 0, '0'),
(60, 10, 50, NULL, 0, 0, 0, '0', 0, '0'),
(61, 10, 16, NULL, 0, 0, 0, '0', 0, '0'),
(62, 10, 24, NULL, 0, 0, 0, '0', 0, '0'),
(63, 10, 20, NULL, 0, 0, 0, '0', 0, '0'),
(64, 10, 1, NULL, 0, 0, 0, '0', 0, '0'),
(65, 10, 60, NULL, 0, 0, 0, '0', 0, '0'),
(67, 10, 61, NULL, 0, 0, 0, '0', 0, '0'),
(68, 10, 36, NULL, 0, 0, 0, '0', 0, '0'),
(69, 10, 39, NULL, 0, 0, 0, '0', 0, '0'),
(70, 10, 58, NULL, 0, 0, 0, '0', 0, '0'),
(71, 10, 25, NULL, 0, 0, 0, '0', 0, '0'),
(72, 10, 22, NULL, 0, 0, 0, '0', 0, '0'),
(73, 10, 49, NULL, 0, 0, 0, '0', 0, '0'),
(74, 10, 14, NULL, 0, 0, 0, '0', 0, '0'),
(75, 10, 30, NULL, 0, 0, 0, '0', 0, '0'),
(76, 10, 6, NULL, 0, 0, 0, '0', 0, '0'),
(77, 10, 43, NULL, 0, 0, 0, '0', 0, '0'),
(78, 12, 64, 'A', 0, 0, 0, '0', 0, '0'),
(79, 12, 53, NULL, 0, 0, 0, '0', 0, '3');

-- --------------------------------------------------------

--
-- Struktur dari tabel `referee`
--

CREATE TABLE `referee` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `referee`
--

INSERT INTO `referee` (`id`, `name`, `description`) VALUES
(3, 'Max Metino', '#1'),
(4, 'Fransino Tirta', '#2'),
(5, 'Stanley Pesik', '#3'),
(6, 'David', '#4'),
(7, 'Zuli', '#5'),
(8, 'Roly', '#6'),
(9, 'Andre Saputra', '#7'),
(10, 'Hoky Jayanata', '#8'),
(11, 'Alexander Sumardi', '#9'),
(12, 'Arifin Sutanto', '#10'),
(13, 'Roy Laksono', '#11'),
(14, 'Rudi Sandjaja', '#12'),
(15, 'Yohan Mulia', '#13');

-- --------------------------------------------------------

--
-- Struktur dari tabel `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `role` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `role`
--

INSERT INTO `role` (`id`, `role`) VALUES
(1, 'Administrator'),
(2, 'Member');

-- --------------------------------------------------------

--
-- Struktur dari tabel `setting`
--

CREATE TABLE `setting` (
  `year` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `regular_time` int(11) NOT NULL,
  `semifinal_time` int(11) NOT NULL,
  `final_time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `setting`
--

INSERT INTO `setting` (`year`, `month`, `city_id`, `regular_time`, `semifinal_time`, `final_time`) VALUES
(2019, 12, 2, 1, 8, 10);

-- --------------------------------------------------------

--
-- Struktur dari tabel `sub_menu`
--

CREATE TABLE `sub_menu` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `url` varchar(128) NOT NULL,
  `icon` varchar(128) NOT NULL,
  `is_active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sub_menu`
--

INSERT INTO `sub_menu` (`id`, `menu_id`, `title`, `url`, `icon`, `is_active`) VALUES
(1, 1, 'User', 'admin/user', 'fas fa-fw fa-users', 1),
(2, 2, 'My Profile', 'profile', 'fas fa-fw fa-user', 1),
(3, 3, 'Menu Management', 'sidebar/menu', 'fas fa-fw fa-folder', 1),
(4, 3, 'Submenu Management', 'sidebar/sub_menu', 'fas fa-fw fa-folder-open', 1),
(8, 2, 'Edit Profile', 'profile/edit', 'fas fa-fw fa-user-edit', 1),
(9, 1, 'Role', 'admin/role', 'fas fa-fw fa-user-tie', 1),
(12, 39, 'Division', 'master/division', 'fas fa-layer-group fa-fw', 1),
(13, 39, 'Country', 'master/country', 'far fa-flag fa-fw', 1),
(14, 39, 'Club', 'master/club', 'fas fa-fw fa-cube', 1),
(15, 39, 'Player', 'master/player', 'fas fa-users fa-fw', 1),
(16, 39, 'Winning', 'master/winning', 'fas fa-star fa-fw', 1),
(17, 40, 'Player Division', 'entry/player_division', 'fa fa-user-plus fa-fw', 1),
(18, 40, 'Log Match', 'entry/log_match', 'fa fa-fw fa-bullseye', 1),
(19, 39, 'Referee', 'master/referee', 'fa fa-fw fa-user-clock', 1),
(20, 41, 'Setting', 'setting', 'fa fa-fw fa-cog', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `password` varchar(256) NOT NULL,
  `role_id` int(11) NOT NULL,
  `is_active` int(1) NOT NULL,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `image`, `password`, `role_id`, `is_active`, `date_created`) VALUES
(11, 'admin', 'admin@mail.com', 'bg1.jpg', '$2y$10$gnQySwHuNb/Z3ub3yXatQe6fzQcVWYWGqHBADxdKOgiBUwlWu.udu', 1, 1, 1570632753),
(12, 'member', 'member@mail.com', 'default.jpg', '$2y$10$NvjLnbwjZe90ehhBSbS2f.czJXDXRO1zRyLDkPRrYyrrR5jqPUk/C', 2, 1, 1570877973),
(17, 'user@mail.com', 'user@mail.com', 'default.jpg', '$2y$10$KB68XjO58T0YsHhf5E2sbO1FGWN.APFHJAJbgXfvmMMPHxJDflnpK', 2, 1, 1575378676);

-- --------------------------------------------------------

--
-- Struktur dari tabel `winning`
--

CREATE TABLE `winning` (
  `id` int(11) NOT NULL,
  `winning` varchar(100) NOT NULL,
  `point` int(11) DEFAULT NULL,
  `description` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `winning`
--

INSERT INTO `winning` (`id`, `winning`, `point`, `description`) VALUES
(1, 'NULL', NULL, ''),
(2, 'REVERSAL', NULL, ''),
(3, 'TAKE DOWN', NULL, '3 points'),
(4, 'GUARD PASS', NULL, ''),
(5, 'MOUNT', NULL, ''),
(6, 'BACKMOUNT', NULL, ''),
(7, 'WO', NULL, ''),
(8, 'DRAW', NULL, ''),
(9, 'PENALTY', NULL, ''),
(10, 'CARD', NULL, 'Jika lawan dikasih kartu remi'),
(11, 'SUBMISSION', 5, 'Menang jika mematikan lawan');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `access_menu`
--
ALTER TABLE `access_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `achievement`
--
ALTER TABLE `achievement`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `club`
--
ALTER TABLE `club`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `division`
--
ALTER TABLE `division`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `log_match`
--
ALTER TABLE `log_match`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `player`
--
ALTER TABLE `player`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `player_division`
--
ALTER TABLE `player_division`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `referee`
--
ALTER TABLE `referee`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `sub_menu`
--
ALTER TABLE `sub_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `winning`
--
ALTER TABLE `winning`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `access_menu`
--
ALTER TABLE `access_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT untuk tabel `achievement`
--
ALTER TABLE `achievement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT untuk tabel `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `club`
--
ALTER TABLE `club`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=165;

--
-- AUTO_INCREMENT untuk tabel `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT untuk tabel `division`
--
ALTER TABLE `division`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT untuk tabel `log_match`
--
ALTER TABLE `log_match`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1985;

--
-- AUTO_INCREMENT untuk tabel `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT untuk tabel `player`
--
ALTER TABLE `player`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT untuk tabel `player_division`
--
ALTER TABLE `player_division`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT untuk tabel `referee`
--
ALTER TABLE `referee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `sub_menu`
--
ALTER TABLE `sub_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT untuk tabel `winning`
--
ALTER TABLE `winning`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
